Source: optee-client
Priority: optional
Maintainer: Ying-Chun Liu (PaulLiu) <paulliu@debian.org>
Build-Depends: debhelper-compat (= 12),
               docbook-xml,
               docbook-xsl,
               dpkg-dev (>= 1.22.5),
               pkgconf,
               uuid-dev,
               xsltproc
Standards-Version: 4.5.1
Section: libs
Homepage: https://github.com/OP-TEE/optee_client
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/optee-client
Vcs-Git: https://salsa.debian.org/debian/optee-client.git

Package: optee-client-dev
Section: libdevel
Architecture: arm64 armhf
Multi-Arch: same
Depends: libckteec0t64 (= ${binary:Version}),
         libseteec0t64 (= ${binary:Version}),
         libteeacl0.1.0t64 (= ${binary:Version}),
         libteec2 (= ${binary:Version}),
         ${misc:Depends}
Description: normal world user space client APIs for OP-TEE (development)
 OP-TEE is a Trusted Execution Environment (TEE) designed as companion to a
 non-secure Linux kernel running on Arm; Cortex-A cores using the TrustZone
 technology. OP-TEE implements TEE Internal Core API v1.1.x which is the API
 exposed to Trusted Applications and the TEE Client API v1.0, which is the
 API describing how to communicate with a TEE. This package provides the TEE
 Client API library.
 .
 This package contains the development files OpTEE Client API

Package: libteeacl0.1.0t64
Provides: ${t64:Provides}
Replaces: libteeacl0.1.0
Breaks: libteeacl0.1.0 (<< ${source:Version})
Architecture: arm64 armhf
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: normal world user space client APIs for OP-TEE
 OP-TEE is a Trusted Execution Environment (TEE) designed as companion to a
 non-secure Linux kernel running on Arm; Cortex-A cores using the TrustZone
 technology. OP-TEE implements TEE Internal Core API v1.1.x which is the API
 exposed to Trusted Applications and the TEE Client API v1.0, which is the
 API describing how to communicate with a TEE. This package provides the TEE
 Client API library.
 .
 This package contains libteeacl library.

Package: libteec2
Provides: ${t64:Provides}
Replaces: libteec1
Breaks: libteec1 (<< ${source:Version})
Architecture: arm64 armhf
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: normal world user space client APIs for OP-TEE
 OP-TEE is a Trusted Execution Environment (TEE) designed as companion to a
 non-secure Linux kernel running on Arm; Cortex-A cores using the TrustZone
 technology. OP-TEE implements TEE Internal Core API v1.1.x which is the API
 exposed to Trusted Applications and the TEE Client API v1.0, which is the
 API describing how to communicate with a TEE. This package provides the TEE
 Client API library.
 .
 This package contains libteec library.

Package: libckteec0t64
Provides: ${t64:Provides}
Replaces: libckteec0
Breaks: libckteec0 (<< ${source:Version})
Architecture: arm64 armhf
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: normal world user space client APIs for OP-TEE
 OP-TEE is a Trusted Execution Environment (TEE) designed as companion to a
 non-secure Linux kernel running on Arm; Cortex-A cores using the TrustZone
 technology. OP-TEE implements TEE Internal Core API v1.1.x which is the API
 exposed to Trusted Applications and the TEE Client API v1.0, which is the
 API describing how to communicate with a TEE. This package provides the TEE
 Client API library.
 .
 This package contains libckteec library.

Package: libseteec0t64
Provides: ${t64:Provides}
Replaces: libseteec0
Breaks: libseteec0 (<< ${source:Version})
Architecture: arm64 armhf
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: normal world user space client APIs for OP-TEE
 OP-TEE is a Trusted Execution Environment (TEE) designed as companion to a
 non-secure Linux kernel running on Arm; Cortex-A cores using the TrustZone
 technology. OP-TEE implements TEE Internal Core API v1.1.x which is the API
 exposed to Trusted Applications and the TEE Client API v1.0, which is the
 API describing how to communicate with a TEE. This package provides the TEE
 Client API library.
 .
 libseteec stands for secure element control.
 .
 This package contains libseteec library.

Package: tee-supplicant
Architecture: arm64 armhf
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: normal world user space client APIs for OP-TEE
 OP-TEE is a Trusted Execution Environment (TEE) designed as companion to a
 non-secure Linux kernel running on Arm; Cortex-A cores using the TrustZone
 technology. OP-TEE implements TEE Internal Core API v1.1.x which is the API
 exposed to Trusted Applications and the TEE Client API v1.0, which is the
 API describing how to communicate with a TEE. This package provides the TEE
 Client API library.
 .
 This package contains tee-supplicant executable.
